#!/usr/bin/env python3

#~ PACs que han proporcionado un entorno de pruebas libre y abierto
#~ ecodex, finkok
# ~ PAC = 'ecodex'

PASS_KEY = '12345678a'
NAME_TEST = 'cert_test.{}'
CERT_NUM = '20001000000300022815'
RFC_TEST = 'LAN7008173R5'

#~ Soportado actualmente: json, txt
FORMAT_FILES = 'txt'


#~ IMPORTANTE: NO modifiques los valores anteriores, en producción el número de
#~ serie del certificado se obtiene dinámicamente.

